import { Text } from '@chakra-ui/react'
import { FC } from 'react'
import { CheckboxLayout, Square, Tx } from './styled.ts'

interface Step {
  step: number
  currentStep: number
  label: string
  size?: string
}

export const Step: FC<Step> = ({ step, currentStep, label, size }) => {
  let bgColor
  let content

  if (currentStep === step) {
    bgColor = 'rgb(144, 198, 247)'
  } else if (currentStep < step) {
    bgColor = 'rgb(188, 232, 214)'
    content = '✓'
  } else {
    bgColor = 'rgb(223, 223, 223)'
  }

  return (
    <CheckboxLayout>
      <input type='checkbox' />
      <div style={{ position: 'relative' }}>
        <Square style={{ backgroundColor: bgColor }}></Square>
        <Tx>{content || currentStep}</Tx>
      </div>
      <Text fontSize={size || 'xl'}>{label}</Text>
    </CheckboxLayout>
  )
}
