import styled from '@emotion/styled'

export const CheckboxLayout = styled.div`
  display: flex;
  align-items: center;
  gap: 15px;

  input {
    display: none;
  }
`

export const Square = styled.div`
  text-align: center;
  width: 30px;
  height: 30px;
  border-radius: 5px;
`

export const Tx = styled.span`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  color: white;
`
