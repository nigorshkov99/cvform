import {
  useCallback,
  useState,
  ChangeEvent,
  DragEvent,
  useRef,
  forwardRef,
  useImperativeHandle,
  ForwardRefExoticComponent,
  RefAttributes,
  PropsWithoutRef,
} from 'react'

interface FileUploadProps {
  file: File | null
  selectedFile: (p: File | null) => void
  onClick: () => void
}

const FileUpload: ForwardRefExoticComponent<
  PropsWithoutRef<FileUploadProps> & RefAttributes<HTMLInputElement>
> = forwardRef(({ file, selectedFile, onClick }, ref) => {
  const fileInputRef = useRef<HTMLInputElement>(null)

  useImperativeHandle(ref, () => fileInputRef.current as HTMLInputElement, [])

  const [dragging, setDragging] = useState<boolean>(false)

  const onFileChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
    selectedFile(event.target.files ? event.target.files[0] : null)
    event.target.value = ''
  }, [])

  const onDragOver = useCallback((event: DragEvent<HTMLDivElement>) => {
    event.preventDefault()
  }, [])

  const onDrop = useCallback((event: DragEvent<HTMLDivElement>) => {
    event.preventDefault()
    selectedFile(event.dataTransfer.files ? event.dataTransfer.files[0] : null)
  }, [])

  const onDragEnter = useCallback((event: DragEvent<HTMLDivElement>) => {
    event.preventDefault()
    setDragging(true)
  }, [])

  const onDragLeave = useCallback((event: DragEvent<HTMLDivElement>) => {
    event.preventDefault()
    setDragging(false)
  }, [])

  return (
    <div
      onClick={onClick}
      onDragOver={onDragOver}
      onDrop={onDrop}
      onDragEnter={onDragEnter}
      onDragLeave={onDragLeave}
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '250px',
        width: '160px',
        border: '1px dashed gray',
      }}
    >
      <label>
        <input
          style={{ display: 'none' }}
          type='file'
          onChange={onFileChange}
          ref={fileInputRef}
        />
      </label>
      {file ? (
        <img
          style={{ width: '100%', height: '100%' }}
          src={URL.createObjectURL(file)}
          alt='preview'
        />
      ) : (
        <span style={{ color: 'rgb(166,163,163)' }}>
          {dragging ? 'Отпустите' : 'Переместите файл'}
        </span>
      )}
    </div>
  )
})

FileUpload.displayName = 'FileUpload'

export default FileUpload
