import styled from '@emotion/styled'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 600px;
  padding: 30px;
  background-color: white;
`

export const HR = styled.hr`
  width: 100%;
  margin: 20px 0 40px 0;
  border: 1px solid rgb(223, 223, 223);
`

export const ButtonContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;
  align-items: center;
  margin-top: 30px;
`
