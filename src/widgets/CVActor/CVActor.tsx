import { ButtonContainer, HR, Wrapper } from './styled.ts'
import { Steps } from '../../entires/Steps/Steps.tsx'
import { CVPhotoLoader } from '../../entires/CVPhotoLoader/CVPhotoLoader.tsx'
import { useCallback, useEffect, useRef, useState } from 'react'
import { CVForm } from '../../entires/CVForm/CVForm.tsx'
import { Button } from '@chakra-ui/react'
import { FormRef } from '../../types.ts'

export const CVActor = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null)
  const [warningPhoto, setWarningPhoto] = useState<string>('')
  const [step, setStep] = useState<number>(1)

  const formRef = useRef<FormRef>(null)
  const stepRef = useRef(step)

  const handleButtonClick = () => {
    if (!selectedFile) setWarningPhoto('Фото обязательно')

    if (formRef.current) {
      formRef.current.submitForm()
    }
  }

  const handleSetFile = useCallback((file: File | null) => {
    if (!file) return
    setWarningPhoto('')
    setSelectedFile(file)
  }, [])

  const handleOnDelete = useCallback(() => {
    setSelectedFile(null)
  }, [])

  useEffect(() => {
    stepRef.current = step
  }, [step])

  const onSubmit = (data: unknown) => {
    console.log(data) //data - получаем данные с формы можем куда-то добавить или как-то манипулировать
    if (!selectedFile) {
      setWarningPhoto('Фото обязательно')
      return
    }

    if (stepRef.current === 3) {
      setStep(-1)
      return
    }

    setStep((prevStep) => prevStep + 1)
  }

  return (
    <Wrapper>
      <Steps currentStep={step} />
      <HR />
      {step === 1 && (
        <>
          <CVPhotoLoader
            selectedFile={selectedFile}
            setSelectedFile={handleSetFile}
            onDelete={handleOnDelete}
            warning={warningPhoto}
          />
          <CVForm ref={formRef} onSubmit={onSubmit} />
        </>
      )}
      {step === 2 && <div>Шаг 2</div>}
      {step === 3 && <div>Шаг 3</div>}
      {step === -1 && <div>X</div>}
      <ButtonContainer>
        <Button
          colorScheme='twitter'
          isDisabled={step === -1}
          onClick={handleButtonClick}
        >
          Следующий шаг
        </Button>
      </ButtonContainer>
    </Wrapper>
  )
}
