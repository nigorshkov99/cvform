export interface PhotoLoader {
  selectedFile: File | null
  setSelectedFile: (file: File | null) => void
  onDelete: () => void
  warning: string
}

export type FormRef = {
  submitForm: () => void
} | null
