import './App.css'
import { CVActor } from './widgets/CVActor/CVActor.tsx'

function App() {
  return (
    <>
      <CVActor />
    </>
  )
}

export default App
