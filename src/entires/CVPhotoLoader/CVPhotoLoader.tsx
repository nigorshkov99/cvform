import {
  ButtonContainer,
  CVPhotoLoaderContainer,
  SettingLoad,
} from './styled.ts'
import FileUploaders from '../../components/FileUploaders/FileUploaders.tsx'
import { Button, Text } from '@chakra-ui/react'
import { FC, useCallback, useRef } from 'react'
import { PhotoLoader } from '../../types.ts'

export const CVPhotoLoader: FC<PhotoLoader> = ({
  selectedFile,
  setSelectedFile,
  onDelete,
  warning,
}) => {
  const fileInputRef = useRef<HTMLInputElement>(null)

  const onAddClick = useCallback(() => {
    fileInputRef.current?.click()
  }, [])

  return (
    <CVPhotoLoaderContainer>
      <div>
        <FileUploaders
          file={selectedFile}
          selectedFile={setSelectedFile}
          onClick={onAddClick}
          ref={fileInputRef}
        />
        {warning && <Text color='red'>{warning}</Text>}
      </div>
      <SettingLoad>
        <Text color='gray' width='250px' flexWrap='wrap' fontSize='sm'>
          Добавьте сюда свое фото в формате
          <br /> png или jpeg. Ознакомьтесь с нашим фотогидом, чтобы подобрать{' '}
          <br />
          качественный снимок
        </Text>
        <ButtonContainer>
          <Button onClick={onAddClick} colorScheme='blue' variant='outline'>
            Добавить
          </Button>
          <Button onClick={onDelete} colorScheme='gray' variant='outline'>
            Удалить
          </Button>
        </ButtonContainer>
      </SettingLoad>
    </CVPhotoLoaderContainer>
  )
}
