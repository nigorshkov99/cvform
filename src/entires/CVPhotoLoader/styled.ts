import styled from '@emotion/styled'

export const CVPhotoLoaderContainer = styled.div`
  display: flex;
  gap: 50px;
`

export const SettingLoad = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

export const ButtonContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
`
