import styled from '@emotion/styled'

export const StepsContainer = styled.div`
  display: flex;
  gap: 55px;
`
