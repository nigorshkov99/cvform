import { Step } from '../../components/Step/Step.tsx'
import { FC } from 'react'
import { StepsContainer } from './styled.ts'

interface Steps {
  currentStep: number
}

interface StepsData {
  id: number
  label: string
}
export const Steps: FC<Steps> = ({ currentStep }) => {
  //Можно получать, например, с сервера разное кол-во шагов.
  const steps: StepsData[] = [
    { id: 1, label: 'Анкета' },
    { id: 2, label: 'Шаг 2' },
    { id: 3, label: 'Шаг 3' },
  ]

  return (
    <StepsContainer>
      {steps.map((step: StepsData) => {
        return (
          <Step
            key={step.id}
            step={currentStep}
            currentStep={step.id}
            label={step.label}
          />
        )
      })}
    </StepsContainer>
  )
}
