import styled from '@emotion/styled'

export const FormCV = styled.form`
  display: flex;
  flex-direction: column;
  width: 85%;
  margin: 30px;
  gap: 15px;
`

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 5px;
`

export const DateOfBirthday = styled.div`
  display: flex;
  gap: 5px;
`
