import { useForm, Controller } from 'react-hook-form'
import { Input, Text } from '@chakra-ui/react'
import { DateOfBirthday, FormCV, InputContainer } from './styled.ts'
import {
  forwardRef,
  ForwardRefExoticComponent,
  PropsWithoutRef,
  RefAttributes,
  useEffect,
} from 'react'
import { FormRef } from '../../types.ts'

interface CVFormProps {
  onSubmit: (data: unknown) => void
}

export const CVForm: ForwardRefExoticComponent<
  PropsWithoutRef<CVFormProps> & RefAttributes<FormRef>
> = forwardRef(({ onSubmit }, ref) => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const handleFormSubmit = (data: unknown) => {
    onSubmit(data)
  }

  useEffect(() => {
    if (ref && typeof ref !== 'function') {
      ref.current = {
        submitForm: () => handleSubmit(handleFormSubmit)(),
      }
    }
  }, [ref, handleSubmit, handleFormSubmit])

  return (
    <FormCV onSubmit={handleSubmit(handleFormSubmit)}>
      <>
        <InputContainer>
          <Text>Фамилия</Text>
          <Controller
            name='firstName'
            control={control}
            defaultValue=''
            rules={{ required: 'Это поле обязательно' }}
            render={({ field }) => (
              <Input style={{ width: '100%' }} {...field} />
            )}
          />
          {errors.firstName && errors.firstName.message && (
            <Text color='red'>{errors.firstName.message as string}</Text>
          )}
        </InputContainer>
        <InputContainer>
          <Text>Имя</Text>
          <Controller
            name='secondName'
            control={control}
            defaultValue=''
            rules={{ required: 'Это поле обязательно' }}
            render={({ field }) => (
              <Input style={{ width: '100%' }} {...field} />
            )}
          />
          {errors.secondName && errors.secondName.message && (
            <Text color='red'>{errors.secondName.message as string}</Text>
          )}
        </InputContainer>
        <InputContainer>
          <Text>Дата рождения</Text>
          <DateOfBirthday>
            <Controller
              name='year'
              control={control}
              defaultValue=''
              rules={{ required: 'Это поле обязательно' }}
              render={({ field }) => (
                <Input style={{ width: '60%' }} {...field} />
              )}
            />
            {/*month*/}
            <Controller
              name='month'
              control={control}
              defaultValue=''
              rules={{ required: 'Это поле обязательно' }}
              render={({ field }) => (
                <Input style={{ width: '20%' }} {...field} />
              )}
            />
            {/*day*/}
            <Controller
              name='day'
              control={control}
              defaultValue=''
              rules={{ required: 'Это поле обязательно' }}
              render={({ field }) => (
                <Input style={{ width: '20%' }} {...field} />
              )}
            />
          </DateOfBirthday>
          {(errors.year || errors.month || errors.day) && (
            <Text color='red'>{'Это поле обязательно'}</Text>
          )}
        </InputContainer>
      </>
    </FormCV>
  )
})

CVForm.displayName = 'CVForm'
